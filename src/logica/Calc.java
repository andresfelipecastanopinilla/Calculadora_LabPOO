package logica;

public class Calc {
	private int num1;
	private int num2;
	
	public Calc(int num1, int num2) {
		this.num1 = num1;
		this.num2 = num2;
	}

	public int getNum1() {
		return num1;
	}
	
	public void setNum1(int num1) {
		this.num1 = num1;
	}
	
	public int getNum2() {
		return num2;
	}
	
	public void setNum2(int num2) {
		this.num2 = num2;
	}
	
	
	public int sumar() {
		int sumar = this.num1 + this.num2;
		return sumar;
	}
	public int restar() {
		int restar = this.num1 - this.num2;
		return restar;
	}
	public int multiplicar() {
		int multiplicar = this.num1 * this.num2;
		return multiplicar;
	}
	public double dividir() {
		double dividir = (double)this.num1 / (double)this.num2;
		return dividir;
	}
	
	public int potencia() {
		int potencia = (int) Math.pow(num1, num2);
		return potencia;
	}
	
	public int factNum1() {
		int n;
		int cm = 1;
		for(n=1; n<= num1; n++) {
			cm *= n;
		}
		int fact1 = cm;
		return fact1;
	}
	
	public int factNum2() {
		int n;
		int cm = 1;
		for(n=1; n<= num2; n++) {
			cm *= n;
		}
		int fact2 = cm;
		return fact2;
	}
}
