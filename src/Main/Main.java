package Main;

import logica.Calc;

public class Main {
	
	public static void main(String[] args) {
		Calc cl;
		cl = new Calc(5,6);
		int suma = cl.sumar();
		System.out.println("La suma es de: "+ suma);
		int resta = cl.restar();
		System.out.println("La resta es de: "+ resta);
		int mult = cl.multiplicar();
		System.out.println("El producto es de: " + mult);
		double div = cl.dividir();
		System.out.println("La division es de: " + div);
		int potencia = cl.potencia();
		System.out.println("La potencia de " + cl.getNum1() + " elevado a " + cl.getNum2() + " es de " + potencia);
		int factNum1 = cl.factNum1();
		System.out.println("El factorial de " + cl.getNum1() + " es " + factNum1);
		int factNum2 = cl.factNum2();
		System.out.println("El factorial de " + cl.getNum2() + " es " + factNum2);
	}
	
}